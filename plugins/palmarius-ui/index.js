import Vue from 'vue'
import Button from './src/button'
import Link from './src/link'
import Input from './src/input'
import InputPlain from './src/input-plain'
import Select from './src/select'
import Dropdown from './src/dropdown'
import Dayrange from './src/dayrange'
import Cells from './src/cells'
import Cell from './src/cell'
import Cascader from './src/cascader'
import Popup from './src/popup'
import InfinityCalendar from './src/infinity-calendar'
import SmsCode from './src/sms-code'
import Spacer from './src/spacer'
import RadioGroup from './src/radio-group'
import Pager from './src/pager'
import Breadcrumbs from './src/breadcrumbs'
import TabsSlider from './src/tabs-slider'
import Rating from './src/rating'
import Tabs from './src/tabs/tabs'
import TabPane from './src/tabs/tab-pane'
import TagsChevron from './src/tags-chevron'

const components = [
	Button,
	Link,
	Input,
	InputPlain,
	Select,
	Dropdown,
	Dayrange,
	Cells,
	Cell,
	Cascader,
	Popup,
	InfinityCalendar,
	SmsCode,
	Spacer,
	RadioGroup,
	Pager,
	Breadcrumbs,
	TabsSlider,
	Tabs,
	TabPane,
	Rating,
	TagsChevron
]

components.forEach((component) => {
	Vue.component(component.name, component)
})
