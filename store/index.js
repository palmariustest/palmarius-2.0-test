import { apiGlobal } from '@/api/global'
// import { apiHotel } from '@/api/hotel'
import { apiMain } from '@/api/main'
export const strict = false
export const state = () => ({
	gosessid: null,
	user: null,
	language: 'ru',
	currency: 'RUB',
	promo: '',
	languages: {
		ru: {
			name: 'Русский',
			code: 'ru'
		},
		en: {
			name: 'English',
			code: 'en'
		}
	},
	currencies: [
		{ code: 'RUB' },
		{ code: 'ILS' },
		{ code: 'KZT' },
		{ code: 'USD' },
		{ code: 'UAH' },
		{ code: 'PLN' },
		{ code: 'EUR' },
		{ code: 'GBP' }
	],
	phalcon_url: null,
	localization: {},
	directions: [],
	backgrounds: [],
	phone_mask: [],
	loading: false,
	user_location: {
		name: 'Иркутск'
	},
	pagetitle: null,
	subtitle: null,
	tours: [],
	slideImagesData: [],
	toursSpec: [],
	toursPopular: [],
	subdomains: ['hotel', 'piligrim', 'pilgrim'],
	subdomain: '',
	host: null,
	cordova: false,
	icons: {},
	path: ''

})

export const mutations = {
	SET_STATES(state, payload) {
		const keys = Object.keys(payload)
		keys.forEach((key) => {
			state[key] = payload[key]
		})
	},
	SET_LANGUAGE(state, data) {
		state.language = data
	},
	SET_CURRENCY(state, data) {
		state.currency = data
	},
	SET_MAIN_PAGE(state, data) {
		let tours = []
		const toursSpec = []; const toursPopular = []; let slideImagesData; let pagetitle; let subtitle
		try {
			pagetitle = data.intl.title
			subtitle = data.intl.subtitle
			tours = data.objects.tours
			if (state.subdomain === 'hotel') {
				// slideImagesData = [
				// 	{ country: 'TH',
				// 		currency: 'USD',
				// 		image: null,
				// 		name: 'Бар',
				// 		tournumber: '45&nbsp;предложений',
				// 		url: '/tours/destination/tailand' },
				// 	{ country: 'IL',
				// 		currency: 'USD',
				// 		image: null,
				// 		name: 'Room сервис',
				// 		tournumber: '38&nbsp;предложений',
				// 		url: '/tours/destination/izrail' },
				// 	{ country: 'RU',
				// 		currency: 'RUB',
				// 		image: null,
				// 		name: 'Популярное',
				// 		tournumber: '14&nbsp;предложений',
				// 		url: '/tours/destination/irkutsk' },
				// 	{ country: 'RU',
				// 		currency: 'RUB',
				// 		image: null,
				// 		name: 'Экскурсии из отеля',
				// 		tournumber: '12&nbsp;предложений',
				// 		url: '/tours/destination/sankt-peterburg' }
				// ]
			} else {
				slideImagesData = data.objects.directions
			}
		} catch (err) {
			console.log(err)
		}
		tours.forEach((el, key) => {
			if (key < 6) {
				toursSpec.push(el)
			} else {
				toursPopular.push(el)
			}
		})
		// console.log('LOOK HERE', slideImagesData)
		state.toursSpec = toursSpec
		state.toursPopular = toursPopular
		state.tours = tours
		state.slideImagesData = slideImagesData
		state.pagetitle = pagetitle
		state.subtitle = subtitle
	},
	SET_ROUTE_PATH(state, data) {
		state.path = data
	}

}

export const actions = {
	// nuxt client init use for cordova
	async nuxtClientInit({ commit, dispatch }, { req, res, redirect, route, app, context, error, params, router }) {
		console.log('CLIENT INIT')
		// await console.log('CLIENT INIT', { req, res, redirect, route, app, context, error, params, router })
		const server = false
		if (!app.$cookies.get('language')) {
			let chosenLocalization = navigator.language || navigator.userLanguage
			chosenLocalization = chosenLocalization.split('-')[0]
			app.$cookies.set('language', chosenLocalization)
			const host = window.location.host
			const urlParts = host.split('.').filter((el) => { return el })
			const isDev = app.context.isDev
			let subdomain = urlParts.length === 3 || (isDev && urlParts.length === 2) ? urlParts[0] : null
			const domain = isDev ? urlParts[urlParts.length - 1] : urlParts[urlParts.length - 2]
			const zone = isDev ? null : urlParts[urlParts.length - 1]
			const subUrl = route.path.split('/').filter((el) => { return el.length })
			const queries = Object.keys(route.query).map((el) => {
				return route.query[el] ? el + '=' + route.query[el] : el
			})
			if (!subUrl[0]) {
				subUrl.push(chosenLocalization)
			} else if (subUrl[0].length !== 2) {
				subUrl.unshift(chosenLocalization)
			} else if (subUrl[0] !== chosenLocalization) {
				subUrl[0] = chosenLocalization
			}
			if (subdomain && !['hotel', 'pilgrim', 'piligrim'].includes(subdomain)) {
				subdomain = null
			}
			const urlString = (subdomain ? subdomain + '.' : '') + (domain && domain !== 'undefined' ? domain : '') + (zone ? '.' + zone + '/' : '/') + subUrl.join('/') + (queries.length > 0 ? '?' + queries.join('&') : '')
			redirect(urlString)
		}
		if (app.context.env.isCordova === 'true') {
			const cordova = true
			await dispatch('initMyApp', { req, res, redirect, route, app, context, error, params, server, cordova, router })
			return true
		}
	},
	// nuxt server init use for web
	async nuxtServerInit({ commit, dispatch, rootState, state }, { req, res, redirect, route, app, context, error, params, router }) {
		const server = true
		const cordova = false
		await dispatch('initMyApp', { req, res, redirect, route, app, context, error, params, server, cordova, router })
	},
	// initialization of all variables
	async initMyApp({ commit, dispatch, rootState, state }, { req, res, redirect, route, app, context, error, params, server, router, cordova }) {
		console.log('IS SERVER', server)
		let needRedirect = false
		// api
		const apiStore = await apiGlobal.getApiStore(this.$axios)
		const languages = await apiGlobal.getLanguages(this.$axios)
		const languageCodes = languages.map(el => el.code)
		let chosenLocalization = app.$cookies.get('language')
		// url
		const host = server ? req.headers.host : window.location.host
		const urlParts = host.split('.').filter((el) => { return el })
		const isDev = app.context.isDev
		let subdomain = urlParts.length === 3 || (isDev && urlParts.length === 2) ? urlParts[0] : null
		const domain = isDev ? urlParts[urlParts.length - 1] : urlParts[urlParts.length - 2]
		const zone = isDev ? null : urlParts[urlParts.length - 1]
		const subUrl = route.path.split('/').filter((el) => { return el.length })
		const queries = Object.keys(route.query).map((el) => {
			return route.query[el] ? el + '=' + route.query[el] : el
		})
		console.log('FIRST', host, urlParts, subdomain)
		// other variables
		let hotel = null
		let backgrounds = [
			{ id: 0, image: '/static-assets/images/main_bg_2.jpeg' },
			{ id: 1, image: '/static-assets/images/main_bg_3.jpeg' },
			{ id: 2, image: '/static-assets/images/main_bg_1.jpeg' }
		]

		// logics
		// localization
		// if (!chosenLocalization) {
		// 	if (!server) {
		// 		chosenLocalization = navigator.language || navigator.userLanguage
		// 		chosenLocalization = chosenLocalization.split('-')[0]
		// 		console.log('navigator lang', chosenLocalization)
		// 	} else {
		// 		chosenLocalization = 'en'
		// 	}
		// }

		// last checkin
		if (languageCodes.indexOf(chosenLocalization) < 0) {
			chosenLocalization = 'en'
		}

		// analize is need redirect
		if (!subUrl[0]) {
			subUrl.push(chosenLocalization)
			needRedirect = true
		} else if (subUrl[0].length !== 2) {
			subUrl.unshift(chosenLocalization)
			needRedirect = true
		} else if (subUrl[0] !== chosenLocalization) {
			subUrl[0] = chosenLocalization
			needRedirect = true
		}
		// set cookies
		// app.$cookies.set('language', chosenLocalization)

		// check subdomain
		if (subdomain) {
			if (state.subdomains.indexOf(subdomain) < 0) {
				subdomain = null
				needRedirect = true
			} else if (['pilgrim', 'piligrim'].indexOf(subdomain) >= 0) {
				backgrounds = [
					{ id: 0, image: '/static-assets/images/pilgrim/pilgrim_background.jpg' }
				]
				commit('search/SET_STATES', {
					tags: 1
				})
			} else if (subdomain === 'hotel') {
				if (params.hotel) {
					hotel = await dispatch('hotel/GET_HOTEL_BY_SLUG', params.hotel)
					if (!hotel || hotel.error) {
						commit('hotel/SET_STATES', { hotelDialogShown: true }, { root: true })
					} else {
						commit('hotel/SET_STATES', {
							hotel: hotel.hotel,
							hotelServices: hotel.service_classes
						}, { root: true })
						const image = 'https://tickets.palmarius.org/images/' + hotel.hotel.logo + '?w=' + 1920 + '&h=' + 800 + '&blur=' + 0
						backgrounds = [
							{ id: 0, image: image }
						]
					}
				}
			}
			console.log('yes subdomain', subdomain)
		} else {
			console.log('no subdomain', subdomain)
		}

		// make a redirect if need it
		console.log('SECOND', host, urlParts, subdomain)
		if (needRedirect) {
			console.warn('REDIRECT')
			const urlString = (subdomain !== null ? (subdomain + '.') : '') + (domain && domain !== 'undefined' ? domain : '') + (zone ? '.' + zone + '/' : '/') + subUrl.join('/') + (queries.length > 0 ? '?' + queries.join('&') : '')
			console.log('urlString', urlString)
			if (cordova) {
				console.log('I AM CORDOVA')
			} else { redirect(urlString) }
		}
		// get localization from api
		await dispatch('GET_LOCALIZATION', chosenLocalization)
		// get session id
		let sessionId = app.$cookies.get('GOSESSID')
		if (!sessionId) {
			sessionId = await dispatch('GET_SESSION_ID', domain)
		}
		const languageObjects = {}
		languages.forEach((el) => {
			languageObjects[el.code] = el
		})
		await dispatch('GET_ICONS_ALL')

		await dispatch('GET_PHONE_MASK')
		await dispatch('GET_USER')
		// init new tour draft
		if (state.user) {
			await dispatch('editTour/GET_DRAFT', state.user.role_list[0].id, { root: true })
		}
		// set states
		commit('SET_STATES', {
			language: chosenLocalization,
			languages: languageObjects,
			currency: apiStore.currency,
			subdomain,
			host,
			backgrounds,
			gosessid: sessionId,
			cordova: cordova
		})
		// set filters
		let filters = app.$cookies.get('filters')
		if (!filters) {
			filters = {
				filterVacation: [],
				filterIncludes: [],
				filterTickets: [],
				filterPartners: [],
				filterPrice: [1000, 10000]
			}
		}
		commit('search/SET_STATES', filters, { root: true })
		// set api store
		if (apiStore.language !== chosenLocalization) {
			await dispatch('SET_API_STORE_BY_NAME', ['language', chosenLocalization])
		}

		console.log('URL PARTS', { subdomain, domain, zone, queries, languages, backgrounds, languageCodes, subUrl, chosenLocalization, apiStore, needRedirect })
	},
	async GET_SESSION_ID({ commit }, domain) {
		const gosessid = await apiGlobal.getSessionId(this.$axios)
		const expires = new Date()
		expires.setMonth(expires.getMonth() + 1)
		// console.log('store index get session id', domain, gosessid, expires)
		this.$cookies.set('GOSESSID', gosessid, {
			expires
		})
		return gosessid
	},
	async GET_LOCALIZATION({ commit }, payload) {
		const localization = await apiGlobal.getLocales(this.$axios, payload)
		commit('SET_STATES', {
			localization
		})
	},
	async GET_MAIN_PAGE({ commit }) {
		const data = await apiMain.getMainpage(this.$axios)
		console.log('store index get main page', data)
		commit('SET_MAIN_PAGE', data)
	},
	async GET_MAIN_PAGE_PILGRIM({ commit }) {
		const data = await apiMain.getMainpagePilgrim(this.$axios)
		commit('SET_MAIN_PAGE', data)
	},
	async GET_USER({ commit }) {
		await this.$axios.$get('/api/v1.0/session/user').then((res) => {
			// console.log(res)
			if (res.error) {
				commit('SET_STATES', { user: null })
			} else {
				commit('SET_STATES', { user: res })
			}
		}).catch((e) => {
			console.log(e)
		})
	},
	async SET_API_STORE_BY_NAME({ commit }, data) {
		const name = data[0]
		const value = data[1]
		commit('loading/SET_LOADING', true)
		await apiGlobal.putApiStoreByName(this.$axios, name, value).then((res) => {
			console.log('store index set api store by name', res, name, value)
			commit('SET_' + name.toUpperCase(), value)
			commit('loading/SET_LOADING', false)
		}).catch((err) => { console.log(err) })
	},
	async GET_PHONE_MASK({ commit }) {
		await apiGlobal.getPhoneCodes(this.$axios).then((codes) => {
			commit('SET_STATES', {
				phone_mask: codes.map(el => el.mask)
			})
		}).catch((e) => { console.log(e) })
	},
	async GET_DIRECTIONS({ commit }) {
		await apiGlobal.getDirections(this.$axios).then((res) => {
			commit('SET_STATES', { directions: res })
		}).catch((e) => { console.log(e) })
	},
	async GET_CURRENCIES({ commit }) {
		await apiGlobal.getCurrencies(this.$axios).then((res) => {
			// console.log(res)
			commit('SET_CURRENCIES', res)
		}).catch((e) => { console.log(e) })
	},
	async GET_ICONS_ALL({ commit }) {
		const result = await apiGlobal.getIconsAll(this.$axios)
		console.log(result)
		commit('SET_STATES', {
			icons: result.icons
		})
	}
}

export const getters = {
	gosessid(state) {
		return state.gosessid
	},
	user(state) {
		return state.user
	},
	language(state) {
		return state.language
	},
	currency(state) {
		return state.currency
	},
	promo(state) {
		return state.promo
	},
	languages(state) {
		return state.languages
	},
	currencies(state) {
		return state.currencies
	},
	directions(state) {
		return state.directions
	},
	directionsObject(state) {
		const object = {}
		state.directions.forEach((el) => {
			object[el.id] = el
		})
		return object
	},
	phalcon_url(state) {
		return state.phalcon_url
	},
	backgrounds(state) {
		return state.backgrounds
	},
	phone_mask(state) {
		return state.phone_mask
	},
	loading(state) {
		return state.loading
	},
	user_location(state) {
		return state.user_location
	},
	navigation(state) {
		const currLanguage = state.language
		const nav = [
			{},
			{ name: 'ln_partners', url: '/' + currLanguage + '/partners', type: null }
			// { name: 'ln_information_center', url: 'https://ru.palmarius.org/instruction', type: 'phalcon' },
			// { name: 'ln_about_project', url: 'https://ru.palmarius.org/instruction/o-platforme', type: 'phalcon' }
		]
		if (state.subdomain === 'hotel') {
			nav[0] = { name: 'ln_navigation_main_all_services', url: '/' + currLanguage + '/services', type: null }
		} else {
			nav[0] = { name: 'ln_search_tours', url: '/' + currLanguage + '/tours', type: null }
		}

		return nav
	},
	dynamicNavigation(state) {
		let partnerNav = []
		const currLanguage = state.language
		if (state.path.fullPath && state.path.fullPath.includes('account/partner') && state.user) {
			partnerNav = [
				{
					name: 'ln_ac_partner_excursions', url: '/' + currLanguage + '/account/partner/excursions', type: null
				},
				{
					name: 'ln_ac_partner_travel_programs', url: '/' + currLanguage + '/account/partner/', type: null
				},
				{
					name: 'ln_ac_partner_services', url: '/' + currLanguage + '/account/partner/services', type: null
				},
				{
					name: 'ln_ac_partner_reservation', url: '/' + currLanguage + '/account/partner/reservation', type: null
				}, {
					name: 'ln_ac_partner_finance', url: '/' + currLanguage + '/account/partner/excursions', type: null
				},
				{
					name: 'ln_ac_partner_accommodation', url: '/' + currLanguage + '/account/partner/excursions', type: null
				}, {
					name: 'ln_ac_partner_personnel', url: '/' + currLanguage + '/account/partner/excursions', type: null
				}, {
					name: 'ln_ac_partner_organization', url: '/' + currLanguage + '/account/partner/excursions', type: null
				}, {
					name: 'ln_ac_partner_documents', url: '/' + currLanguage + '/account/partner/excursions', type: null
				},
				{
					name: 'ln_ac_partner_invite_partner', url: '/' + currLanguage + '/account/partner/excursions', type: null
				}, {
					name: 'ln_ac_partner_personal_data', url: '/' + currLanguage + '/account/partner/excursions', type: null
				}]
		}

		return partnerNav
	},
	mainPageUrl(state) {
		const currLanguage = state.language
		return '/' + currLanguage
	}
}
