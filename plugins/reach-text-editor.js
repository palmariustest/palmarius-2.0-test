import Vue from 'vue'
import wysiwyg from 'vue-wysiwyg'
Vue.use(wysiwyg, {
	hideModules: {
		'bold': false,
		'italic': false,
		'underline': false,
		'justifyLeft': true,
		'justifyCenter': true,
		'justifyRight': true,
		'headings': true,
		'link': true,
		'code': true,
		'orderedList': true,
		'unorderedList': true,
		'image': true,
		'table': true,
		'removeFormat': true,
		'separator': true
	}
})
