import Cells from './src/cells'

Cells.install = function (Vue) {
	Vue.component(Cells.name, Cells)
}

export default Cells
