import Vue from 'vue'
const Filters = {
	install(Vue) {
		Vue.filter('filterDate', (value) => {
			if (!value) return ''
			const fullDate = new Date(value)
			const year = fullDate.getFullYear()
			const month = fullDate.getMonth() + 1
			const date = fullDate.getDate()
			const result =
				year +
				'-' +
				(month <= 9 ? '0' + month : month) +
				'-' +
				(date <= 9 ? '0' + date : date)
			return result
		})
		Vue.filter('filterMonthDate', (value) => {
			if (!value) return ''
			const fullDate = new Date(value)
			const month = fullDate.getMonth() + 1
			const date = fullDate.getDate()
			return (date <= 9 ? '0' + date : date) + '.' + (month <= 9 ? '0' + month : month)
		})
		Vue.filter('filterDateTime', (value) => {
			if (!value) return ''
			const fullDate = new Date(value)
			const year = fullDate.getFullYear()
			const month = fullDate.getMonth() + 1
			const date = fullDate.getDate()
			const hours = fullDate.getHours()
			const minutes = fullDate.getMinutes()
			const result =
				year +
				'-' +
				(month <= 9 ? '0' + month : month) +
				'-' +
				(date <= 9 ? '0' + date : date) +
				' ' +
				(hours <= 9 ? '0' + hours : hours) +
				':' +
				(minutes <= 9 ? '0' + minutes : minutes)
			return result
		})

		Vue.filter('filterNum', (value) => {
			if (isNaN(value) || !value) {
				value = 0
				return value
			}
			if (value < 0) {
				value = 0
				return value
			}
			value = value.toFixed().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
			return value
		})

		Vue.filter('weekDate', (date, monthes, weekDays) => {
			return `${date.getDate()} ${monthes[date.getMonth()]}, ${weekDays[date.getDay()]}`
		})

		Vue.filter('weekDateWithYear', (date, monthes, weekDays) => {
			return `${date.getDate()} ${monthes[date.getMonth()]} ${date.getFullYear()}, ${weekDays[date.getDay()]}`
		})

		Vue.filter('money', (cost, currencySymbol) => {
			return `${cost.toLocaleString()} ${currencySymbol}`
		})
	}
}
Vue.use(Filters)
