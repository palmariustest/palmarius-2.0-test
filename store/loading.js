export const state = () => ({
	loading: false
})

export const mutations = {
	SET_LOADING(state, boolean) {
		state.loading = boolean
	}
}

export const actions = {

}

export const getters = {
	loading(state) {
		return state.loading
	}
}
