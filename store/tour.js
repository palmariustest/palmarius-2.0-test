import { toursApi } from '@/api/tour'

export const state = () => ({
	tour: null,
	title: null,
	background: null,
	toursNearBy: []
})

export const mutations = {
	SET_STATES(state, payload) {
		// console.log('STATES', payload)
		const keys = Object.keys(payload)
		keys.forEach((key) => {
			state[key] = payload[key]
		})
	},
	SET_TOUR(state, tour) {
		if (!tour) {
			tour = {
				name: null,
				days: [],
				images: [],
				include: [],
				xclude: [],
				discounts: [],
				tags: [],
				types: [],
				services: [],
				owner: {},
				dates: {},
				qa: []
			}
		}
		if (!tour.services) {
			tour.services = []
		}
		state.tour = tour
		state.title = tour.name
		let imageSrc = 'no-image'
		try {
			imageSrc = tour.tour.images[0]
		} catch (err) {}
		state.background = 'https://tickets.palmarius.org/images/' +
        imageSrc +
        '?w=1000&h=650&blur=10'
	},
	// CHOOSE_DATE(state, {}) {
	// 	state.toursNearBy[data[0]].activeDate = data[1]
	// },
	CHOOSE_DATE(state, { tourKey, dateKey }) {
		state.toursNearBy[tourKey].activeDate = dateKey
	},
	CHOOSE_TIME(state, { tourKey, dateKey, time }) {
		state.toursNearBy[tourKey].activeTime = time
	},
	CLEAR(currState) {
		currState = state()
	}
}

export const actions = {
	async fetchTourBySlug({ commit }, slug) {
		const { tour } = await toursApi.fetchTour(this.$axios, { slug })
		commit('SET_TOUR', tour)
	},
	async GET_TOURS_NEAR_BY({ commit }) {
		const tours = await toursApi.getToursNearBy(this.$axios)
		console.log('TOURS', tours)
		commit('SET_STATES', {
			toursNearBy: tours.tours
		})
	}
}

export const getters = {
	// getters
}
