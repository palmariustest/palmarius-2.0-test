import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: '/static-assets/images/image_not_found.png',
	loading: '/static-assets/images/loading_image.svg',
	attempt: 1,
	listenEvents: ['scroll']
})
