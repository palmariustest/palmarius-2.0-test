DOCKER_RUN_NODE ?= docker run -it \
	--rm \
	-v ${PWD}:/home/node/app \
	-w="/home/node/app" \
	-u "node"

NODEJS_IMAGE ?= node:12-slim
NPM = /usr/local/bin/npm

npm:
	$(DOCKER_RUN_NODE) \
	$(NODEJS_IMAGE) \
	/bin/bash

npm-i:
	$(DOCKER_RUN_NODE) \
	$(NODEJS_IMAGE) \
	$(NPM) i

up:
	docker-compose up

upd:
	docker-compose up -d

restart:
	docker-compose restart

log:
	docker logs -f palmarius

down:
	docker-compose down
