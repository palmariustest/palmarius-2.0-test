import Vue from 'vue'
Vue.directive('mask', {
	bind(el, binding, vnode) {
		let phoneMasks = []
		if (binding.value && binding.value.mask) {
			phoneMasks = binding.value.mask
		} else {
			return false
		}
		function setCaretPosition(el, caretPos) {
			if (!el) {
				return false
			}
			if (el.createTextRange) {
				const range = el.createTextRange()
				range.move('character', caretPos)
				range.select()
				return true
			} else if (el.selectionStart || el.selectionStart === 0) {
				el.setSelectionRange(caretPos, caretPos)
				return true
			} else {
				return false
			}
		}
		if (binding.value && binding.value.mask) {
			phoneMasks = binding.value.mask
		} else {
			return false
		}
		const inputHandler = function (e) {
			let inputValue = e.target.value
			console.log('input value', e.inputType, inputValue)
			if (e.inputType === 'deleteContentBackward') {
				return false
			}
			const inputValueArray = inputValue.split('')
			if (inputValue.includes('+89')) {
				inputValueArray[1] = '7'
				inputValue = inputValueArray.join('')
			} else if (inputValue.length === 11 && inputValue.split('')[0] === '8') {
				inputValueArray[0] = '7'
				inputValue = inputValueArray.join('')
			}
			if (!inputValue.split(/ /)[0].replace(/[^\d]/g, '')) {
				e.target.value = ''
				return false
			}
			let caretPosition = null

			function findMask(value) {
				const masks = []
				let result = ''

				let inputValueRegex = value.split(/ /)[0].replace(/[^\d]/g, '')
				phoneMasks.forEach((el) => {
					// REMOVE ALL MASKS WHICH LENGTH SMALLER THEN INPUT VALUE LENGTH
					console.log('LENGTH', inputValueRegex, el)
					if (el.replace(/[^\d^#]/g, '').length < inputValueRegex.length) {
						return false
					}
					let elRegex = el.replace(/[^\d]/g, '')

					if (inputValueRegex.length >= elRegex.length) {
						inputValueRegex = inputValueRegex.slice(0, elRegex.length)
					} else {
						elRegex = elRegex.slice(0, inputValueRegex.length)
					}
					if (inputValueRegex === elRegex) {
						masks.push(el)
					}
				})
				masks.forEach((mask) => {
					if (mask.replace(/[^\d]/g, '').length > result.replace(/[^\d]/g, '').length) {
						result = mask
					}
				})
				return result
			}
			let longestMask = findMask(inputValue)

			function masking(mask, value) {
				let specSymbolCounter = 0
				const specSymbol = ['+', '-', '(', ')']
				let result = ''
				const inputValueRegex = value.split(/ /)[0].replace(/[^\d]/g, '')
				mask.split('').forEach((el, key) => {
					if (specSymbol.indexOf(el) >= 0) {
						result = result + el
						specSymbolCounter++
					} else if (inputValueRegex[key - specSymbolCounter]) {
						result = result + inputValueRegex[key - specSymbolCounter]
					} else {
						result = result + '_'
						if (!caretPosition) {
							caretPosition = key
							// console.log('CARPOS', key)
						}
					}
				})
				return result
			}
			let inputValueResult = ''
			if (longestMask.length > 0) {
				inputValueResult = masking(longestMask, inputValue)
			} else {
				const testInputValue = inputValue.slice(0, inputValue.length - 1)
				longestMask = findMask(testInputValue)
				if (longestMask.length === testInputValue.length) {
					inputValueResult = testInputValue
				} else {
					// console.log('DO NOTHING')
					return false
				}
			}
			if (!caretPosition) {
				caretPosition = inputValueResult.replace(/[^\d^+-^(^)]/g, '').length
				// console.log('CARET POS NON', caretPosition, inputValueResult, inputValueResult.replace(/[^\d^+-^^(^)]/g, ''))
			}
			if ('()-'.includes(e.target.value[caretPosition - 1])) {
				caretPosition--
				// console.log('CARET POSITION MINS', caretPosition)
			}
			e.target.value = inputValueResult
			if (longestMask) {
				// console.log('CARET POSITION', caretPosition)
				setCaretPosition(e.target, caretPosition)
			}
		}

		el.addEventListener('input', inputHandler)
	}
})
