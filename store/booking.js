import Vue from 'vue'

import { isEqualDates } from '@/utils'
import { bookingApi } from '@/api/booking'

export const state = () => ({
	tourId: null,
	selectedDate: null,
	availableDates: [],
	peoples: {
		adults: 1,
		children: []
	},
	needTickets: false,
	tickets: {
		fetching: false,
		foundTickets: null
	},
	tourCostDetails: {
		fetching: false,
		details: null
	},
	services: [],
	mobileStep: 'info',
	bookingForm: {
		date: null,
		tour: null,
		tour_date: null,
		conditions: 1,
		contact_phone: null,
		contact_email: null,
		ticket_scan: null,
		passport_birthdate: '18.10.1966',
		passport_country: 'RU',
		passport_firstname: 'Василий',
		passport_secondname: 'Иваныч',
		passport_lastname: 'Пупкинг',
		passport_sernum: '1324 4653745',
		passport_xpire: '18.10.1966',
		person: [],
		isWhatsapp: false
	},
	bookingDialog: false
})

export const mutations = {
	SET_STATES(state, payload) {
		const keys = Object.keys(payload)
		keys.forEach((key) => {
			state[key] = payload[key]
		})
	},
	SET_MOBILE_STEP(state, step) {
		state.mobileStep = step
	},
	SET_NEED_TICKETS_FLAG(state, needTickets) {
		state.needTickets = needTickets
	},
	SET_PEOPLES(state, { adults, children }) {
		state.peoples = {
			adults,
			children: children.map(ch => ({ age: ch.age }))
		}
	},
	SET_TOUR_ID(state, tourId) {
		state.tourId = tourId
	},
	SET_AVAILABLE_DATES(state, dates) {
		state.availableDates = [...dates]
	},
	SET_SELECTED_DATE(state, date) {
		state.selectedDate = date
	},
	ADD_SERVICE({ services }, { service, tour }) {
		services.push({ ...service, currency_conv: tour.currency_conv })
	},
	REMOVE_SERVICE({ services }, service) {
		const targetServiceId = service.id
		const targetServiceIndex = services.findIndex(
			service => service.id === targetServiceId
		)
		services.splice(targetServiceIndex, 1)
	},
	SEARCH_TICKETS_REQUEST_SENT(state) {
		state.tickets.fetching = true
		state.tickets.foundTickets = null
	},
	SEARCH_TICKETS_REQUEST_SUCCESS(state, foundTickets) {
		state.tickets = {
			fetching: false,
			foundTickets
		}
	},
	GET_TOUT_COST_DETAILS_REQUEST_SENT(state) {
		state.tourCostDetails.fetching = true
		state.tourCostDetails.details = null
	},
	GET_TOUT_COST_DETAILS_REQUEST_SUCCESS(state, details) {
		state.tourCostDetails = {
			fetching: false,
			details
		}
	},
	CLEAR_TOUR_COST_DETAILS(currState) {
		currState.tourCostDetails = state().tourCostDetails
	},
	CLEAR_TICKETS(currState) {
		currState.tickets = state().tickets
	},
	CLEAR_SERVICES(state) {
		Vue.set(state, 'services', [])
	},
	CLEAR_STATE(currentState) {
		currentState = state()
	}
}

export const actions = {
	toBooking({ commit }) {
		commit('SET_MOBILE_STEP', 'booking')
	},
	backToTourInfo({ commit }) {
		commit('SET_MOBILE_STEP', 'info')
	},
	monthChanged({ commit }, { month, year }) {
		commit('SET_SELECTED_DATE', null)
		commit('CLEAR_TICKETS')
	},
	dateSelected({ commit, dispatch }, date) {
		commit('SET_SELECTED_DATE', date)
		dispatch('loadCost')
	},
	peoplesChanged({ commit, dispatch }, peoples) {
		commit('SET_PEOPLES', peoples)
		dispatch('loadCost')
	},
	loadCost({ dispatch }) {
		dispatch('loadTickets')
		dispatch('loadTourCostDetails')
	},
	async loadTickets({ commit, getters, state }) {
		if (state.needTickets && getters.allPeopleCanGo) {
			commit('SEARCH_TICKETS_REQUEST_SENT')
			const tickets = await bookingApi.fetchTickets(this.$axios, {
				tourId: state.tourId,
				startDate: state.selectedDate,
				peoples: state.peoples
			})
			commit('SEARCH_TICKETS_REQUEST_SUCCESS', tickets)
		} else {
			commit('CLEAR_TICKETS')
		}
	},
	async loadTourCostDetails({ getters, commit, state }) {
		if (getters.allPeopleCanGo) {
			commit('GET_TOUT_COST_DETAILS_REQUEST_SENT')
			const details = await bookingApi.fetchTourCost(this.$axios, {
				tourId: state.tourId,
				startDate: state.selectedDate,
				peoples: state.peoples
			})
			commit('GET_TOUT_COST_DETAILS_REQUEST_SUCCESS', details)
		} else {
			commit('CLEAR_TOUR_COST_DETAILS')
		}
	},
	userNeedsTickets({ commit, dispatch }) {
		commit('SET_NEED_TICKETS_FLAG', true)
		dispatch('loadCost')
	},
	userDoesNotNeedTickets({ commit }) {
		commit('SET_NEED_TICKETS_FLAG', false)
		commit('CLEAR_TICKETS')
	},
	removeService({ commit }, service) {
		commit('REMOVE_SERVICE', service)
	}
}

export const getters = {
	availableDates(state) {
		return state.availableDates.map(({ date }) => new Date(date))
	},
	selectedDatePositions(state) {
		if (!state.selectedDate) {
			return null
		}
		const targetDate = state.availableDates.find(({ date }) =>
			isEqualDates(new Date(date), state.selectedDate)
		)
		return targetDate ? targetDate.people_left : null
	},
	selectedDatePrice(state) {
		if (!state.selectedDate) {
			return null
		}
		const targetDate = state.availableDates.find(({ date }) =>
			isEqualDates(new Date(date), state.selectedDate)
		)
		console.log('TARGET DATE', targetDate)
		return targetDate ? targetDate.price_conv : null
	},
	totalPeoplesCount(state) {
		return state.peoples.adults + state.peoples.children.length
	},
	allPeopleCanGo(state, getters) {
		return typeof getters.selectedDatePositions === 'number'
			? getters.totalPeoplesCount <= getters.selectedDatePositions
			: null
	},
	additionalServiceIsVisible(state) {
		return state.services.length || state.tickets.foundTickets
	},
	hasBonus({ tourCostDetails, tickets }) {
		return Boolean(
			(tourCostDetails.details && tourCostDetails.details.totalDiscount) ||
				(tickets.foundTickets && tickets.foundTickets.bonusMiles)
		)
	}
}
