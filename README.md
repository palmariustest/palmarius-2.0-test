# ![Palmarius][logo-black] 2.0 VueSite


## API documentations

[API docs at ![Google Docs][google-drive]][api-dox]

## Build Setup

``` bash
# install dependencies
# NPM                  Yarn               Nuxt
npm install            yarn               nuxt

# serve with hot reload at localhost:3000
npm run dev            yarn dev           nuxt dev

# build for production and launch server
npm run build          yarn build         nuxt build
npm start              yarn start         nuxt start

# generate static project
npm run generate       yarn generate      nuxt generate
```

- [Nuxt.js docs](https://nuxtjs.org).
- [Element UI docs](lnk-elmui-doc), [repository ![][git-ico]]()


---

[lnk-nuxt]: https://ru.nuxtjs.org/guide
[lnk-elmui-doc]: https://element.eleme.io/#/en-US/component/quickstart
[lnk-elmui-rep]:https://github.com/ElemeFE/element

[logo-black]: /static/images/logo_black.png
[api-dox]: https://docs.google.com/document/d/1bNwH0cnImZQNA94qdnBaAAn_ji4b2MezbXqgPDj8Ecs/edit?usp=sharing

[google-drive]: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAgCAIAAAD1803ZAAAC3UlEQVR42mP8//8/AzHg40GGi+74FOjvZOC3x28GI7GWAcFFV4aPh3HKcusxGJ+inmU/HjCc0sCnQLmbQTqXfMsefnk9/cq2Dot4GL+Z4WErTtXM/AzmNxlYBMi0LPPQ9OV3Dh0KaNcTUgDx/3xgOGvG8PMRTg3S2QzKveRYdundA7sNlUCGjYTWFq9aqOjbTQxXw/AFptFJBh59ki3z3tZ09MV1CHuqbUa0Kiyx4U8p/HYM+rtIs+zw82u+25vhXDkeUWBgCrBxgzhfLjKcM8fnOfVZDOJxJFhms6H8yjuUuCk3DK40DIFy7hYzPJ2K0zJ2eQbjk5gpBbtlS28fzD48Az142LgOBXTI84iCOMCUclKN4e8nnPbJVzPI16KJYbdMd2Xu469vMMW95UyWuhRDOS8XMdxMwxeYZjcYOBQIWNZ+fk3n+bW4TNjsWWsrqQXlnDVl+HoZp2UYKQXdsg+/vuqvyvv46xtRlpFYYKJbVnFi4YxrO3DptZbQ3OpVhyJ0M4Xh5RKclgFTCrBMwWoZsHACeguPQ1G8BQGkpBQUyyCFEy5dkSp20+0ysUgQLDCB2QCcUhCWoeViTHAxbBI03WMCoOfwFJjiMQzqc1AsQy6cMAFKjsYExKUUqGVbHp6O2duHSyUfK+el8MnQsgoXuBrC8HYLTllw1Qq1DFcuJspbEEBE1QqyDGvhBAey3CKXwyczUAMwvv/5BX8uRqlcKLSs7dxqPIWTjpDckYBOCLt29Y9jt/+SbVOWCxvjklsH8IQhPBdfePi3eOkPsm0S52NclsMFijNciR65cCpc8v3So39kW1bmw+6uxwKyDFd2hudiqniLAZ6pMQsq5MKJKt5CWAasWfRW5n76/R0iDczFhwM7qestBuTiCrnORM7FFHqrN5rDQJ4Z3TIGWDmCXDhR6C09Oab+GE44F8UySEpBzsVU9BYDZk0NTCnwdLHz0p+uLT+p5S0gAAC0hZu6JRUQVQAAAABJRU5ErkJggg==

[git-ico]: data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAbCAIAAADganh7AAADQUlEQVR42q2WX0haURzHz71qmqmZXTPo4QqNehlrsOHdQ7iH0PWwYg8VPmTtIWFQsaC9ZcQe2kttDNqg0TZYttG0weZ6CKuBshddL8aCjAaaBTl0Wvn35tV586Y3s7qNfd/Oj3M//H7n/M73dyEcx0EREXF/xLKCWzYJWwBs5MIsoETg9qsl6pt8lMcBZwg6BSVC7v2peXw4AM6XsoatbxMpxZwLoVH7XLhjLe0DTNVyo3Tqrkh8JjQZMb4OdzHnHQuVcSw9EpRdBBoxTv4LMSuZjGN7IEFPQnHnXBBbo0KqGlYDIEw7wHMuqK4S1iHA5Eo5jpaKep5FU87LQ90B5bukg9oMmwelagEA4QPLQky3lkbLIIUczmbh2iVWQ8CHsKc1Ioy8ogPDeFQXob4c60b65awsdM88ud+RL5xlHUEwpnXjtpmg+tfxqpLj7pNUk9Ct3ZZXh4v5fbB1RMoYGjG+CHfRmm+sW9YvB9DWt235cpq277h8ZlDzZJhWJbjWwHfcE0JLb735/AHQ3hFN3SplmmhG/j/al4em3LKc4xqQQG+eeHMnfck0ix3r0ZVAo0PeYXAixPhAKdlnfbdduRVkfFj1/6HTvYVQMtSBXIpZ0ACwZUgKzTzz0ntChQm+NpddghkO6p7iBlqt9syZ/vjgzT1QUlzYOiDFeAyRCftsiFY7ADUluz0V0L5jR/wllY3UscAGAWTlLEOnSImUXJTjgXk+2u8CdBdqaRKYGssgPPpbP5oYzzpNG8ezHtcvkx6BciGVHMaucFXXhdV5W4t7VqJmN7HqTlki4JSpwcZBaauAfPsxz4K/3k7GSK6GbZuL6XaoXUXewmZA8T65Wix1WS3X1SnmUS4VD+mfJ8YTFGVCnDBvHy6upwCPrdVUKgvfwt7Y4/hwESZk7K1qPeocyqTj6371R4J0Py7L2leBATwUz0BhICgVA0bQ9iaBoZFqm5zzE57vAeUyNZ3aa1kKsgFgRbMEY5Ap3aHBycGXcH7ea3XSp17Rt1AIVaAlhvsVKC1SME2J0M+g9hOxyBiqxfgTzcKCtoaK/ExkBslS7JEzvXEulPncpykZc20nZXLhqYvK/G2EfQi/XnDmH8pf8PSIprGPchkAAAAASUVORK5CYII=
