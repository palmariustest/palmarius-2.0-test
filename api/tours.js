const getTours = ($axios, params) => {
	let results = {
		intl: {},
		pager: {},
		tours: []
	}
	return $axios.$get('/api/v1.0/tours', {
		params: params
	}).then((res) => {
		results = res
		return Promise.resolve(results)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(results)
	})
}
const getToursByDirection = ($axios, direction, pager) => {
	let results = {
		intl: {},
		pager: {},
		tours: []
	}
	return $axios.$get('/api/v1.0/tours/by-direction/' + direction, {
		params: {
			limit: pager.limit,
			page: pager.page
		}
	}).then((res) => {
		results = res
		return Promise.resolve(results)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(results)
	})
}
const getToursByOperator = ($axios, operator, limit) => {
	let results = {
		intl: {},
		pager: {},
		tours: []
	}
	return $axios.$get('/api/v1.0/tours/by-operator/' + operator, {
		params: {
			limit: limit
		}
	}).then((res) => {
		results = res
		return Promise.resolve(results)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(results)
	})
}
const getDirectionIdByName = ($axios, name) => {
	return $axios.$get('api/v1.0/library/direction/auto?search=' + name).then((res) => {
		const result = {
			id: null,
			name: null
		}
		console.log('RES', res)
		if (res.city) {
			try {
				result.id = res.city[0].children[0].children[0].id
			} catch (err) { console.log(err) }
			try {
				result.name = res.city[0].children[0].children[0].name
			} catch (err) { console.log(err) }
		}
		return result
	})
}

const getTourDayTags = ($axios) => {
	return $axios.$get('api/v1.0/tours/tags').then((res) => {
		return res
	}).catch((err) => {
		console.log(err)
	})
}

const getMyTours = ($axios, { onwner, lang }) => {
	return $axios.$get(`api/v1.0/tours/partner/${onwner}/${lang}`).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
export const apiTours = {
	getTours,
	getToursByDirection,
	getToursByOperator,
	getDirectionIdByName,
	getTourDayTags,
	getMyTours
}
