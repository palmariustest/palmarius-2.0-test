const getDraft = ($axios, partnerId) => {
	return $axios.$get(`/api/v1.0/tours/vue/edit/new/${partnerId}`).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}

const updateDraft = ($axios, payload) => {
	return $axios.$put(`/api/v1.0/tours/vue/update/new`, {
		...payload
	}).then((res) => {
		return Promise.resolve({ status: true })
	}).catch((err) => {
		console.log(err)
		return Promise.resolve({ status: false })
	})
}

const getTour = ($axios, id) => {
	return $axios.$get(`/api/v1.0/tours/vue/edit/${id}`).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}

const updateTour = ($axios, { id, tour }) => {
	return $axios.$put(`/api/v1.0/tours/vue/update/${id}`, {
		...tour
	}).then((res) => {
		return Promise.resolve({ status: true })
	}).catch((err) => {
		console.log(err)
		return Promise.resolve({ status: false })
	})
}

const saveTour = ($axios, id) => {
	return $axios.$post(`/api/v1.0/tours/vue/save/${id}`).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}

const deleteTour = ($axios, id) => {
	return $axios.$delete(`/api/v1.0/tours/${id}`).then((res) => {
		return Promise.resolve({ status: true })
	}).catch((err) => {
		console.log(err)
		return Promise.resolve({ status: false })
	})
}

const unpublishTour = ($axios, id) => {
	return $axios.$put(`/api/v1.0/tours/vue/toggle/${id}`, {
		'active': false
	}).then((res) => {
		return Promise.resolve({ status: true })
	}).catch((err) => {
		console.log(err)
		return Promise.resolve({ status: false })
	})
}

const publishTour = ($axios, id) => {
	return $axios.$put(`/api/v1.0/tours/vue/toggle/${id}`, {
		'active': true
	}).then((res) => {
		return Promise.resolve({ status: true })
	}).catch((err) => {
		console.log(err)
		return Promise.resolve({ status: false })
	})
}

export const apiEditTour = {
	getDraft,
	updateDraft,
	getTour,
	updateTour,
	deleteTour,
	unpublishTour,
	publishTour,
	saveTour

}
