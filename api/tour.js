
export const mockTour = () => ({
	// нет в апи, хардкод
	tags: [
		{
			id: 1,
			name: 'Активный отдых'
		},
		{
			id: 2,
			name: 'Туризм'
		},
		{
			id: 3,
			name: 'Пляжный отдых'
		},
		{
			id: 4,
			name: 'Культурные программы'
		}
	],
	avg_expenses: 100 // нет в апи

})
const getToursNearBy = ($axios) => {
	return $axios.$get('/api/v1.0/tours/search', {
		params: {
			page: 1,
			limit: 3
		}
	}).then((res) => {
		if (res.tours) {
			res.tours.forEach((el) => {
				el.activeTime = null
				// el.dates.forEach((elDate) => {
				// 	elDate.start_time = ['10:00', '15:00', '20:00']
				// })
				if (el.dates[0]) {
					if (el.dates[0].start_time && el.dates[0].start_time[0]) {
						el.activeTime = el.dates[0].start_time[0]
					}
				}
			})
		}
		return res
	}).catch((err) => {
		console.log(err)
	})
}
const fetchTour = ($axios, { slug } = {}) => {
	return $axios.$get(`/api/v1.0/tours/details/${slug}`).then((res) => {
		console.log('TOUR', res)
		if (!res.tour) return res
		// мока для данных которые пока не возвращает бэк
		res.tour = { ...res.tour, ...mockTour() }
		// if (!res.tour.services) {
		// 	res.tour.services = [
		// 		{
		// 			id: 0,
		// 			title: 'Service title',
		// 			popular: true,
		// 			description: 'Service description',
		// 			price_conv: 1000,
		// 			personal: true
		// 		},
		// 		{
		// 			id: 1,
		// 			title: 'Service title',
		// 			popular: false,
		// 			description: 'Service description',
		// 			price_conv: 200,
		// 			personal: false
		// 		}
		// 	]
		// }
		if (!res.tour.tags) {
			res.tour.tags = []
		}

		if (!res.tour.visa) {
			res.tour.visa = true
		}
		if (!res.tour.days) {
			res.tour.days = []
		}
		if (!res.tour.avg_expenses) {
			res.tour.avg_expenses = 100
		}
		const datesKeys = Object.keys(res.tour.dates)
		const now = new Date()
		const filteredDates = {}
		datesKeys.forEach((el) => {
			if (new Date(el) >= now) {
				filteredDates[el] = res.tour.dates[el]
			}
		})
		res.tour.dates = filteredDates
		return res
	})
}

export const toursApi = {
	fetchTour,
	getToursNearBy
}
