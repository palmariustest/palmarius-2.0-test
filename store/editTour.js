import {
	apiEditTour
} from '@/api/editTour.js'

export const state = () => ({
	draft: {},
	tour: {}

})

export const mutations = {
	SET_DRAFT(state, payload) {
		state.draft = Object.assign({}, state.draft, payload)
	},
	SET_TOUR(state, payload) {
		state.tour = Object.assign({}, state.tour, payload)
	}

}

export const actions = {

	async GET_DRAFT({ commit }, partnerId) {
		const {
			tour
		} = await apiEditTour.getDraft(this.$axios, partnerId)
		if (!tour.program.days) {
			tour.program.days = [{
				'id': 5596,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			},
			{
				'id': 5597,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			},
			{
				'id': 5598,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			},
			{
				'id': 5599,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			}
			]
		}

		tour.program.days = tour.program.days.map((day, index) => {
			day.dayNumber = ++index
			return day
		})

		commit('SET_DRAFT', tour)
	},
	async GET_TOUR({ commit }, tourId) {
		const {
			tour
		} = await apiEditTour.getTour(this.$axios, tourId)

		if (!tour.program.days) {
			tour.program.days = [{
				'id': 5596,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			},
			{
				'id': 5597,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			},
			{
				'id': 5598,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			},
			{
				'id': 5599,
				'program': null,
				'description': '',
				'images': null,
				'tags': null
			}
			]
		}

		tour.program.days = tour.program.days.map((day, index) => {
			day.dayNumber = ++index
			return day
		})
		commit('SET_TOUR', tour)
		return tour
	},

	UPDATE_DRAFT({
		commit,
		state
	}, data) {
		const ownerId = state.draft.owner
		const tour = {
			owner: ownerId,
			section: 1, // 1 - тур 2 - экскуосия 3 - услуга отеля
			...data
		}
		// console.log(tour)
		// console.log('Уходит в SET_DRAFT', data)

		commit('SET_DRAFT', data)
		apiEditTour.updateDraft(this.$axios, tour)
	},
	UPDATE_TOUR({
		commit,
		state
	}, { program, id }) {
		// покаа не реализует бэк будет 3(30 01 2021)
		const ownerId = state.tour.owner || 3
		const tour = {
			owner: ownerId,
			section: 1,
			program
		}

		commit('SET_TOUR', { program })
		apiEditTour.updateTour(this.$axios, { id: id, tour: tour })
	},

	async SAVE_TOUR({ commit }, tourId) {
		await apiEditTour.saveTour(this.$axios, tourId)
		// commit('SET_TOUR', tour)
	},

	async UNPUBLISH_TOUR({ commit }, tourId) {
		await apiEditTour.unpublishTour(this.$axios, tourId)
	},
	async PUBLISH_TOUR({ commit }, tourId) {
		await apiEditTour.publishTour(this.$axios, tourId)
	},
	async DELETE_TOUR({ commit }, tourId) {
		await apiEditTour.deleteTour(this.$axios, tourId)
	}

}

export const getters = {
	draft(state) {
		return state.draft
	},
	tour(state) {
		return state.tour
	},
	draftProgram(state) {
		if (state.draft.program) {
			return state.draft.program
		} else {
			return {
				days: []
			}
		}
	},
	tourProgram(state) {
		if (state.tour.program) {
			return state.tour.program
		} else {
			return {
				days: []
			}
		}
	}
}
