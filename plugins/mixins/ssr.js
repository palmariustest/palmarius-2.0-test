import Vue from 'vue'
import { mapState, mapGetters } from 'vuex'

import ClickOutside from 'vue-click-outside'
// import axios from 'axios'
const MixinSsr = {
	install(Vue, Options) {
		Vue.mixin({
			directives: {
				ClickOutside
			},
			data() {
				return {
					WINDOW_WIDTH: 0,
					WINDOW_HEIGHT: 0,
					DIALOG_WIDTH: 0
				}
			},
			computed: {
				...mapState({
					__icons: 'icons',
					__host: 'host',
					__subdomain: 'subdomain',
					__currencies: 'currencies'
				}),
				...mapState('tour', {
					__tour: 'tour'
				}),
				...mapGetters({
					__gosessid: 'gosessid',
					__user: 'user',
					__language: 'language',
					__currency: 'currency',
					__promo: 'promo',
					__languages: 'languages',
					__currencies: 'currencies',
					__directions: 'directions',
					__directionsObject: 'directionsObject',
					__phalcon_url: 'phalcon_url',
					__backgrounds: 'backgrounds',
					__phone_mask: 'phone_mask',
					__user_location: 'user_location'
				}),
				...mapGetters('loading', {
					__loading: 'loading'
				}),
				$FORM_BUTTON_SIZE() {
					if (this.$IS_MOBILE) {
						return 'small'
					} else if (this.$route.name !== 'index') {
						return 'large'
					} else {
						return 'medium'
					}
				},
				$IS_MOBILE() {
					if (process.browser) {
						return this.WINDOW_WIDTH <= 768
					} else {
						return this.$ua.isFromSmartphone()
					}
				},
				$SEARCH_FORM_SHOWN() {
					return this.$store.state.search_form_shown
				},
				$CURRENCY_LIST() {
					return this.$store.state.currency_list
				},
				$WEEK_DAYS() {
					return [
						this.$LOCALE('ln_week_day_short_0'),
						this.$LOCALE('ln_week_day_short_1'),
						this.$LOCALE('ln_week_day_short_2'),
						this.$LOCALE('ln_week_day_short_3'),
						this.$LOCALE('ln_week_day_short_4'),
						this.$LOCALE('ln_week_day_short_5'),
						this.$LOCALE('ln_week_day_short_6')
					]
				},
				$MONTHES() {
					return [
						this.$LOCALE('ln_month_short_0'),
						this.$LOCALE('ln_month_short_1'),
						this.$LOCALE('ln_month_short_2'),
						this.$LOCALE('ln_month_short_3'),
						this.$LOCALE('ln_month_short_4'),
						this.$LOCALE('ln_month_short_5'),
						this.$LOCALE('ln_month_short_6'),
						this.$LOCALE('ln_month_short_7'),
						this.$LOCALE('ln_month_short_8'),
						this.$LOCALE('ln_month_short_9'),
						this.$LOCALE('ln_month_short_10'),
						this.$LOCALE('ln_month_short_11')
					]
				}
			},
			methods: {
				$SHOW_ICON(id, classId) {
					if (this.__icons[id]) {
						return 'https://rest.palmarius.org:10443/static/icons/' + this.__icons[id]
					} else {
						return 'https://rest.palmarius.org:10443/static/icons/' + this.__icons[classId]
					}
				},
				$REMOVE_SPACES(str) {
					return str.replace(/\s/g, '')
				},
				$FIXED_NUM(num) {
					const exponent = Math.pow(10, (num.toString().length - 1))
					const number = (Math.floor(num / exponent)) * exponent
					return number
				},
				$REMOVE_FOCUS() {
					if (process.browser) {
						document.activeElement.blur()
					}
				},
				$TOGGLE_BODY_CLASS(addRemoveClass, className) {
					if (process.browser) {
						const elBody = document.body
						const scroll = window.scrollY
						if (addRemoveClass === 'addClass') {
							elBody.style.top = '-' + scroll + 'px'
							elBody.classList.add(className)
						} else {
							elBody.classList.remove(className)
							const elScroll = Math.abs(parseInt(elBody.style.top))
							elBody.style.top = 'auto'
							window.scrollTo(0, elScroll)
						}
					}
				},
				$SCROLL_TO(el) {
					if (process.browser) {
						document.querySelector(el).scrollIntoView({ behavior: 'smooth' })
					}
				},
				$SHOW_IMAGE(url) {
					if (url) {
						return url
					} else {
						return '/static-assets/images/svg/placeholder.svg'
					}
				},
				$API_IMAGE(src, size, blur) {
					let string = ''
					const _blur = blur ? ('&blur=' + blur) : ''
					if (src) {
						// https://tickets.palmarius.org/images/[filename]?w=[int]&h=[int]&blur=[float]
						string = 'https://tickets.palmarius.org/images/' + src + '?w=' + size[0] + '&h=' + size[1] + _blur
					}
					return string
				},
				$WINDOW_LESS(size) {
					return this.WINDOW_WIDTH <= size
				},
				$NUM_SUFFIX(n, textForms) {
					n = Math.abs(n) % 100; const n1 = n % 10
					if (textForms) {
						if (n > 10 && n < 20) { return textForms[2] }
						if (n1 > 1 && n1 < 5) { return textForms[1] }
						if (n1 === 1) { return textForms[0] }
						return textForms[2]
					} else {
						return null
					}
				},
				$SET_PHALCON_URL(url) {
					this.$store.commit('SET_PHALCON_URL', url)
				},
				$GET_DATE(d) {
					const fullDate = new Date(d)
					const year = fullDate.getFullYear()
					const month = fullDate.getMonth() + 1
					const date = fullDate.getDate()
					const result =
						year +
						'-' +
						(month <= 9 ? '0' + month : month) +
						'-' +
						(date <= 9 ? '0' + date : date)
					return result
				},
				$LOCALE(key) {
					return this.$store.state.localization[key]
				},
				$LAZYLOAD_USER_AVA(src) {
					return {
						src: '/static-assets/images/svg/user-placeholder.svg',
						error: '/static-assets/images/svg/user-placeholder.svg',
						loading: '/static-assets/images/svg/user-placeholder.svg'
					}
				},
				$GO_TO(url) {
					this.$router.push({
						path: url
					})
				},
				$RANDOM_INT(min, max) {
					return Math.floor(Math.random() * (max - min + 1) + min)
				}
			}
		})
	}
}
Vue.use(MixinSsr)
