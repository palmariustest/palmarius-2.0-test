export default function ({ app, store, $axios, route }) {
	if (route.query.promo) {
		store.dispatch('SET_PROMO', route.query.promo)
	}
}
