import { apiPartners } from '@/api/partners'

export const state = () => ({
	hotels: [],
	hotelsTitle: {
		page_intro_title: '',
		page_title: ''
	},
	hotelsPager: {
		total: 0,
		page: 1
	},
	partners: [],
	partnersTitle: {
		page_intro_title: '',
		page_title: ''
	},
	partnersPager: {
		total: 0,
		page: 1
	},
	partnerTitle: null
})

export const mutations = {
	SET_HOTELS(state, data) {
		state.hotels = data.partners
		state.hotelsTitle = data.intl
		state.hotelsPager = data.pager
	},
	SET_PARTNERS(state, data) {
		state.partners = data.partners
		state.partnersTitle = data.intl
		state.partnersPager = data.pager
	},
	SET_PARTNER_TITLE(state, name) {
		state.partnerTitle = name
	},
	CLEAR(currState) {
		currState = state()
	}
}

export const actions = {
	async GET_HOTELS({ commit }, pager) {
		// commit('loading/SET_LOADING', true, { root: true })
		await apiPartners.getHotels(this.$axios, pager).then((res) => {
			// console.log(res)
			commit('SET_HOTELS', res)
		}).catch((err) => { console.log(err) }).finally(() => {
			// commit('loading/SET_LOADING', false, { root: true })
		})
	},
	async GET_PARTNERS({ commit }, pager) {
		// commit('loading/SET_LOADING', true, { root: true })
		const partners = await apiPartners.getPartners(this.$axios, pager)
		// console.log('PARTNERS', partners)
		commit('SET_PARTNERS', partners)
	},
	async GET_PARTNER_BY_SLUG({ commit }, slug) {
		const partner = await apiPartners.getPartnerBySlug(this.$axios, slug)
		commit('SET_PARTNER_TITLE', partner.name)
	}

}

export const getters = {
	// getters
}
