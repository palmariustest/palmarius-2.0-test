import { apiSearch } from '@/api/search'
import { apiMain } from '@/api/main'
import { apiPartners } from '@/api/partners'
export const state = () => ({
	// PAGER
	page: 1,
	limit: 5,
	// SEARCH FORM PROPERTIES
	loc: null,
	dest: 0,
	date: '2019-09-01, 2019-10-01',
	days: [1, 28],
	adults: 1,
	children: [],
	// SLUGS
	countryCode: null,
	directionSlug: null,
	directionName: null,
	partnerSlug: null,
	// FILTERS STATE
	filterPartners: [],
	filterPrice: [100, 100000],
	// FILTERS VALUES
	filterPartnersValues: [],
	// NODE TYPE
	type: 'tour',
	chosenTabType: 'tour',
	// tour, xcursion
	// TOURS
	tours: [],
	pager: {
		page: 1,
		total: 0,
		lastpage: 1
	},
	pagetitle: null,
	dialogShown: false,
	actualTours: true,
	pagetitleEnding: null,
	tags: '',
	searchFormActiveTab: 'VueSearchTourForm'
})
export const mutations = {
	SET_STATES(state, payload) {
		// console.log('STATES', payload)
		const keys = Object.keys(payload)
		keys.forEach((key) => {
			state[key] = payload[key]
		})
	},
	SET_TOURS(state, payload) {
		state.tours = payload
	},
	ADD_TOURS(state, payload) {
		state.tours = state.tours.concat(payload)
	},
	SET_FILTERS_PARTNERS_VALUES(state, payload) {
		state.filterPartnersValues = payload
	},
	SET_PAGER(state, payload) {
		state.pager = payload
	},
	SET_PAGETITLE(state, payload) {
		state.pagetitle = payload
	},
	CHOOSE_DATE(state, { tourKey, dateKey }) {
		state.tours[tourKey].activeDate = dateKey
	},
	CHOOSE_TIME(state, { tourKey, dateKey, time }) {
		// console.log(tourKey, dateKey, time)
		state.tours[tourKey].activeTime = time
	},
	CLEAR(currState) {
		currState = state()
	},
	CLEAR_STATES(currState, payload) {
		payload.forEach((el) => {
			currState[el] = state()[el]
		})
	}
}
export const actions = {
	async SEARCH({ commit, state, dispatch }, payload) {
		const data = await apiSearch.search(this.$axios, payload)
		// console.log('TOURS', payload, data)
		if (!state.directionSlug && data) {
			commit('SET_PAGETITLE', data.intl.pagetitle)
		}
		if (data) {
			commit('SET_STATES', {
				pager: data.pager,
				tours: data.tours,
				actualTours: data.actualTours
			})
		}
	},
	async SEARCH_LOAD_MORE({ commit }, payload) {
		const data = await apiSearch.search(this.$axios, payload)
		console.log('STORE SEARCH LOAD MORE', data)
		if (data) {
			commit('ADD_TOURS', data.tours)
			commit('SET_PAGER', data.pager)
		}
		// console.log('ADD TOURS', data)
	},
	async GET_DIRECTION_NAME({ commit }, payload) {
		const name = await apiSearch.getDirectionBySlug(this.$axios, payload)
		commit('SET_PAGETITLE', name.name)
	},
	async GET_ACTIVE_PARTNERS({ commit, state }, payload) {
		const activePartners = await apiPartners.getActivePartners(this.$axios, payload)
		// console.log('ACTTIVE PARTNERS', activePartners)
		let filteredPartners = activePartners.partners
		try {
			filteredPartners = activePartners.partners.filter((el) => {
				return el.active
			})
		} catch (err) {}
		commit('SET_FILTERS_PARTNERS_VALUES', filteredPartners)
	},
	async GET_POPULAR_TOURS({ commit }) {
		const data = await apiMain.getMainpage(this.$axios)
		commit('SET_STATES', {
			tours: data.objects.tours
		})
	}
}
