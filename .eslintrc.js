module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: "babel-eslint"
  },
  extends: [
    "@nuxtjs",
    "plugin:nuxt/recommended"
  ],
  // add your custom rules here
  rules: {
	"no-tabs": 0,
	"no-console": 0,
	"indent": [2, "tab"],
	"no-v-html": false
	// "vue/html-indent": ["error", "tab", {
	// 	"attribute": 1,
	// 	"baseIndent": 1,
	// 	"closeBracket": 0,
	// 	"alignAttributesVertically": true,
	// 	"ignores": []
	// }]
  }
}
