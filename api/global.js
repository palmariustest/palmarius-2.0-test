const BASE_URL = '/api/v1.0'
function getStaticJSON($axios, url) {
	const isCordova = process.env.isCordova === 'true'

	if (!isCordova) {
		return $axios.$get(`/${url}`)
	}
	return $axios(url, {
		baseURL: ''
	}).then((res) => {
		return typeof res.data === 'string' ? JSON.parse(res.data) : res.data
	})
}

const getApiStore = ($axios) => {
	let apiStore = {
		language: 'ru',
		currency: 'USD',
		promo: ''
	}
	return $axios.$get('/api/v1.0/session/api_store').then((res) => {
		apiStore = res
		return Promise.resolve(apiStore)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(apiStore)
	})
}
const getSessionId = ($axios) => {
	return $axios.$get('/api/v1.0/session/sessid').then((res) => {
		return res.GOSESSID
	}).catch((err) => {
		console.log(err)
	})
}
const putApiStoreByName = ($axios, name, value) => {
	return $axios.$put('/api/v1.0/session/api_store', {
		[name]: value
	}).then((res) => {
		return Promise.resolve({ status: true })
	}).catch((err) => {
		console.log(err)
		return Promise.resolve({ status: false })
	})
}
const getLanguages = ($axios) => {
	let languages = [
		{ code: 'ru' },
		{ code: 'en' }
	]
	return $axios.$get('/api/v1.0/library/lang/active').then((res) => {
		languages = res
		return Promise.resolve(languages)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(languages)
	})
}
const getCurrencies = ($axios) => {
	let currency = [
		{ name: 'USD', code: 'usd' }
	]
	return $axios.$get('/api/v1.0/library/currency/all').then((res) => {
		currency = res
		return Promise.resolve(currency)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(currency)
	})
}
const getDirections = ($axios) => {
	let directions = []
	return $axios.$get('/api/v1.0/library/direction/all').then((res) => {
		directions = res
		return Promise.resolve(directions)
	}).catch((err) => {
		console.log(err)
		return Promise.resolve(directions)
	})
}

const getLocales = ($axios, urlLang) => {
	return getStaticJSON($axios, `lang/${urlLang}.json`)
}

const getPhoneCodes = ($axios) => {
	return getStaticJSON($axios, 'phone-codes.json')
}

const getIconsAll = ($axios) => {
	return $axios.$get(`${BASE_URL}/service/icons/section/direction`).then(res => res).catch(err => console.log(err))
}
export const apiGlobal = {
	getApiStore,
	getSessionId,
	putApiStoreByName,
	getLanguages,
	getCurrencies,
	getDirections,
	getLocales,
	getPhoneCodes,
	getIconsAll
}
