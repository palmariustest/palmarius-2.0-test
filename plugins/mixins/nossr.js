import Vue from 'vue'
const Nossr = {
	install(Vue, Options) {
		Vue.mixin({
			data() {
				return {}
			},
			mounted() {
				this.WINDOW_WIDTH = innerWidth
				this.WINDOW_HEIGHT = innerHeight
				this.DIALOG_WIDTH = this.WINDOW_WIDTH - 100
				if (this.DIALOG_WIDTH > 1000) {
					this.DIALOG_WIDTH = 1000
				}
				this.$nextTick(() => {
					addEventListener('resize', () => {
						this.WINDOW_WIDTH = innerWidth
						this.WINDOW_HEIGHT = innerHeight
						this.DIALOG_WIDTH = this.WINDOW_WIDTH - 100
						if (this.DIALOG_WIDTH > 1000) {
							this.DIALOG_WIDTH = 1000
						}
					})
				})
			},
			// watch: {},
			methods: {

			}
			// created(){},
			// updated(){}
		})
	}
}
Vue.use(Nossr)

// methods: {

//   },
//   mounted() {
// 	this.toggleBodyClass('addClass', 'mb-0');
//   },
//   destroyed() {
// 	this.toggleBodyClass('removeClass', 'mb-0');
//   }
