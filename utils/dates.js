export const isEqualDates = (left, right) => {
	return (left.getDate() === right.getDate()) && (left.getMonth() === right.getMonth()) && (left.getFullYear() === right.getFullYear())
}

export const getStartOfWeek = (date) => {
	const d = new Date(date)
	const day = d.getDay() > 0 ? d.getDay() - 1 : 6 // приведение дней к нумерации с понедельника
	return new Date(d.setDate(d.getDate() - day))
}

export const getEndOfWeek = (date) => {
	const startOfWeek = getStartOfWeek(date)
	return new Date(startOfWeek.setDate(startOfWeek.getDate() + 6))
}
