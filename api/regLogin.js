const sendSmsCode = ($axios, phone) => {
	return $axios.$post('/api/v1.0/lk/smssend', {
		phone: phone
	}).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
const login = ($axios, form) => {
	return $axios.$post('/ajax/login', form, {
		headers: {
			'x-requested-with': 'XMLHttpRequest'
		}
	}).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
const register = ($axios, form) => {
	return $axios.$post('/api/v1.0/lk/register', form).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
export const apiRegLogin = {
	login,
	register,
	sendSmsCode
}
