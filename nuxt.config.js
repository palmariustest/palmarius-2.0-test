const isCordova = process.env.IS_CORDOVA === 'true'

const nuxtConfig = {
	// mode: 'spa',
	// debug: true,
	env: {
		isCordova: JSON.stringify(isCordova)
	},
	head: {
		title: 'Отдых на  море без посредников в 2020 - Купить тур без посредников',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1' },
			// width=device-width, height=device-height, initial-scale=1, maximum-scale=1, target-densityDpi=device-dpi
			{ hid: 'description', name: 'description', content: 'Организуйте свой отдых без посредников. Мы даём простую возможность каждому человеку собрать отдых для себя из различных предложений наших партнеров (отели, тур. компании, частные гиды, транспортные компании).' },
			{ name: 'google-site-verification', content: 'nUQAIi3_wQu8uTtMkHlA8yAVfIhSGNRF49GGfvA1cE8' },
			{ name: 'yandex-verification', content: 'da8048fd85c7c79a' }
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			// { rel: 'stylesheet', href: '/static-assets/plugins/fontawesome5/css/all.min.css' },
			{ rel: 'stylesheet', href: '/static-assets/plugins/animate/animate.min.css' },
			{ rel: 'stylesheet', href: '/static-assets/plugins/pui-icons/css/pui-icon.css' },
			{
				rel: 'stylesheet',
				href:
					'https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,900&display=swap&subset=cyrillic'
			}
		]
	},

	loading: '~/components/loading.vue',

	css: ['swiper/dist/css/swiper.css', '~/assets/styles.scss'],
	styleResources: {
		sass: ['~/assets/vars/*.scss']
	},

	plugins: [
		'@/plugins/axios',
		'@/plugins/element-ui',
		'@/plugins/palmarius-ui',
		'@/plugins/mixins/ssr',
		'@/plugins/mixins/filters',
		{ src: '@/plugins/inputmask', mode: 'client' },
		{ src: '@/plugins/google-maps', mode: 'client' },
		{ src: '@/plugins/vue-swiper', mode: 'client' },
		{ src: '@/plugins/v-body-scroll-lock', mode: 'client' },
		{ src: '@/plugins/mixins/nossr', mode: 'client' },
		{ src: '@/plugins/infinite-loading', mode: 'client' },
		{ src: '@/plugins/vue-lazyload', mode: 'client' },
		{ src: '@/plugins/reach-text-editor', mode: 'client' }
	],

	modules: [
		// nuxt-client-init-module должен быть объявлен самым первым
		// В противном случае на момент выполнения nuxtClientInit
		// другие модули не успеют заинжэектиться в контекст
		'nuxt-client-init-module',
		'@nuxtjs/axios',
		'@nuxtjs/pwa',
		'nuxt-user-agent',
		'@nuxtjs/style-resources',
		'cookie-universal-nuxt', [
			'@nuxtjs/yandex-metrika',
			{
				id: 54372388,
				clickmap: true,
				trackLinks: true,
				accurateTrackBounce: true,
				webvisor: true
			}
		],
		// '@nuxtjs/sitemap'
	],
	sitemap: {
		gzip: true,
		exclude: [
			'/dev/**'
		],
		routes: [
			'/tours'
		],
		cacheTime: 1000 * 60 * 60 * 24
	},
	axios: {
		proxy: true
	},
	proxy: {
		'/api/v1.0': 'https://rest.palmarius.org:10443',
		'/ajax': 'https://rest.palmarius.org:10443',
		'/lang/': 'http://localhost:8000',
		'/phone-codes.json': 'http://localhost:8000'
		/*
		'/tickets': {
			target: 'https://tickets.palmarius.org',
			pathRewrite: { '^/tickets': '' },
			changeOrigin: true,
			prependPath: false
		}
		*/
	},
	// PWA
	workbox: {},
	manifest: {
		name: 'Отдых без посредников от Palmarius',
		short_name: 'Отдых без посредников от Palmarius',
		lang: 'ru',
		icon: '/static/icon.png'
	},
	build: {
		transpile: [/^element-ui/, /^vue2-google-maps($|\/)/],
		extend(config, ctx) {
			// Run ESLint on save
			if (ctx.isDev && ctx.isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/,
					options: {
						fix: true
					}
				})
			}
			// if (ctx.isDev) {
			// 	config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
			// }
		}
	}
}

if (isCordova) {
	const scripts = nuxtConfig.head.script
	nuxtConfig.head.script = scripts ? [...scripts, { src: 'cordova.js' }] : [{ src: 'cordova.js' }]

	const plugins = nuxtConfig.plugins
	nuxtConfig.plugins = plugins ? [...plugins, '@/plugins/cordova.client'] : ['@/plugins/cordova.client']

	nuxtConfig.generate = nuxtConfig.generate
		? { ...nuxtConfig.generate, fallback: 'index.html', dir: 'cordova/www' }
		: { fallback: 'index.html', dir: 'cordova/www' }

	const build = {
		publicPath: '/nuxtfiles/'
	}
	nuxtConfig.build = nuxtConfig.build ? { ...nuxtConfig.build, ...build } : build

	delete nuxtConfig.proxy

	nuxtConfig.axios = {
		baseURL: 'https://rest.palmarius.org:10443',
		browserBaseURL: 'https://rest.palmarius.org:10443'
	}

	nuxtConfig.router = {
		mode: 'hash'
	}
}

export default nuxtConfig
