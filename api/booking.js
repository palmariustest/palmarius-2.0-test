import { isEqualDates, randomInt } from '@/utils'

const availablePositions = ({ month, year }) => {
	const d = new Date()
	d.setMonth(month)
	d.setFullYear(year)

	const dates = []
	const datesMaxCount = randomInt(1, 14)

	for (let i = 0; i < datesMaxCount; i++) {
		d.setDate(randomInt(1, 29))
		const dateIndex = dates.find(found => isEqualDates(new Date(found.date), d))
		if (!dateIndex) {
			dates.push({
				date: d.toISOString(),
				positions: randomInt(1, 100)
			})
		}
	}

	return dates
}

const foundTicketsMock = ({ tourId, startDate, peoples }) => {
	const endDate = new Date(startDate)
	endDate.setDate(startDate.getDate() + 14)
	return {
		there: {
			direction: 'there',
			date: startDate,
			departureFrom: 'KEV',
			arrivalAt: 'TLV',
			departureTime: '4:13',
			arrivalTime: '16:44',
			flyingDuration: '6 ч 30 мин',
			ticketType: '1 пересадка',
			transfer: 'IST'
		},
		back: {
			direction: 'back',
			date: endDate,
			departureFrom: 'TLV',
			arrivalAt: 'KEV',
			departureTime: '7:00',
			arrivalTime: '21:30',
			flyingDuration: '22 ч',
			ticketType: 'Пересадки',
			transfer: 'Прямой'
		},
		bonusMiles: 23000,
		costDetails: {
			adults: {
				count: peoples.adults,
				cost: randomInt(1, 50) * 1000
			},
			children: peoples.children.map(({ age }) => ({
				count: 1,
				age,
				cost: randomInt(1, 10) * 1000
			})),
			administrativeFee: {
				cost: randomInt(1, 3) * 1000
			},
			total: {
				cost: randomInt(30, 40) * 1000
			}
		}
	}
}

const tourCostMock = ({ tourId, peoples }) => {
	return {
		adults: {
			count: peoples.adults,
			cost: randomInt(1, 50) * 1000
		},
		children: peoples.children.map(({ age }) => ({
			count: 1,
			age,
			cost: randomInt(1, 10) * 1000
		})),
		totalDiscount: randomInt(1, 10) * 1000,
		totalCost: randomInt(1, 30) * 1000
	}
}

const fetchAvailableTourPositions = ($axios, { month, year, tourId }) => {
	return Promise.resolve(availablePositions({ month, year }))
}

const fetchTickets = ($axios, { tourId, startDate, peoples }) => {
	// эмулируем сетевую задержку при поиске
	return new Promise((resolve, reject) => {
		setTimeout(
			() => resolve(foundTicketsMock({ tourId, startDate, peoples })),
			800
		)
	})
}

const fetchTourCost = ($axios, { tourId, startDate, peoples }) => {
	return new Promise((resolve, reject) => {
		setTimeout(() => resolve(tourCostMock({ tourId, peoples })), 800)
	})
}

export const bookingApi = {
	fetchAvailableTourPositions,
	fetchTickets,
	fetchTourCost
}
