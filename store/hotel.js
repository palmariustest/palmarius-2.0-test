import { apiHotel } from '@/api/hotel'
export const state = () => ({
	hotel: {
		full_name: null
	},
	searchForm: {
		name: null,
		dest: null,
		daterange: null
	},
	hotelServices: [],
	hotelXcursions: [],
	hotelDialogList: [],
	hotelDialogShown: false,
	hotelDialogFilter: '',
	hotelServiceAutoValues: [],
	hotelServiceSearchValues: [],
	hotelServiceDetailsValue: {},
	hotelServiceDialog: false,
	hotelService: {},
	hotelXcursionSearchValues: []
})

export const mutations = {
	SET_STATES(state, payload) {
		const keys = Object.keys(payload)
		keys.forEach((key) => {
			state[key] = payload[key]
		})
	}
}

export const actions = {
	async GET_HOTELS({ commit }, pager) {
		// commit('loading/SET_LOADING', true, { root: true })
		await apiHotel.getHotels(this.$axios, pager).then((res) => {
			console.log(res.partners)
			commit('SET_STATES', {
				hotelDialogList: res.partners
			})
		}).catch((err) => { console.log(err) }).finally(() => {
			// commit('loading/SET_LOADING', false, { root: true })
		})
	},
	async GET_HOTEL_BY_SLUG({ commit, error }, slug) {
		const result = await apiHotel.getHotel(this.$axios, slug)
		if (result) {
			commit('SET_STATES', {
				hotel: result.hotel,
				hotelServices: result.service_classes,
				hotelDialogShown: false
			})
		}
		return result
	},
	async HOTEL_SERVICE_AUTO({ state, commit }, payload) {
		const result = await apiHotel.hotelServiceAuto(this.$axios, state.hotel.slug, payload)
		console.log('store hotel hotel service auto', result)
		if (!result.error) {
			commit('SET_STATES', {
				hotelServiceAutoValues: result
			})
		} else {
			commit('SET_STATES', {
				hotelServiceAutoValues: []
			})
		}
	},
	async HOTEL_SERVICE_SEARCH({ state, commit }, payload) {
		const query = {}
		const queryKeys = Object.keys(payload.query)
		queryKeys.forEach((el) => {
			if (payload.query[el]) { query[el] = payload.query[el] }
		})
		console.log('SERVICE SEARCH', query, payload)
		const result = await apiHotel.hotelServiceSearch(this.$axios, query, payload.hotel)
		await console.log('hotel service search', result)
		commit('SET_STATES', {
			hotelServiceSearchValues: result
		})
	},
	async HOTEL_SERVICE_DETAILS({ commit }, payload) {
		const result = await apiHotel.hotelServiceDetails(this.$axios, payload)
		commit('tour/SET_STATES', {
			tour: result
		}, { root: true })
	},
	async HOTEL_SERVICE_BOOKING({ commit }, payload) {
		const result = await apiHotel.hotelServiceBooking(this.$axios, payload)
		return result
	},
	async HOTEL_XCURSION_SEARCH({ commit }, payload) {
		const query = {}
		const queryKeys = Object.keys(payload.query)
		queryKeys.forEach((el) => {
			if (payload.query[el]) { query[el] = payload.query[el] }
		})
		const result = await apiHotel.hotelXcursionSearch(this.$axios, payload.hotel, query)
		if (result && result[0]) {
			commit('search/SET_STATES', {
				tours: result
			}, { root: true })
		} else {
			commit('search/SET_STATES', {
				tours: []
			}, { root: true })
		}
		// commit('SET_STATES', { hotelXcursionSearchValues: result })

		if (payload.page === 'main') {
			commit('SET_STATES', { hotelXcursions: result })
		} else if (payload.page === 'tour') {
			commit('tour/SET_STATES', {
				toursNearBy: result
			}, { root: true })
		}
	}
}

export const getters = {
	hotelDialogListFiltered(state) {
		return state.hotelDialogList.filter((el) => {
			return el.name.includes(state.hotelDialogFilter)
		})
	}
}
