const getHotels = ($axios, pager) => {
	return $axios.$get('/api/v1.0/partner/hotels', {
		params: pager
	}).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
const getPartners = ($axios, pager) => {
	return $axios.$get('/api/v1.0/partner/partners', {
		params: pager
	}).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
const getActivePartners = ($axios, payload) => {
	return $axios.$get('/api/v1.0/partner/active_partners', {
		params: payload
	}).then((res) => {
		console.log('ACTIVE PARTNER', payload, res)
		return res
	}).catch((err) => { console.log(err) })
}
const getPartnerBySlug = ($axios, slug) => {
	return $axios.$get('/api/v1.0/partner/by-slug/' + slug).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
export const apiPartners = {
	getHotels,
	getPartners,
	getActivePartners,
	getPartnerBySlug
}
