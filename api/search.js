const search = ($axios, query) => {
	let params = {
		limit: 5
	}
	if (query) {
		params = query
	}
	console.log('params', params)
	return $axios.$get('/api/v1.0/tours/search', {
		params: params
	}).then((res) => {
		// console.log('SEARCH TOURS', res)
		// if (res && res.tours) {
		// 	console.log('YEAH')
		// 	res.tours.forEach((el) => {
		// 		el.activeTime = null
		// 		// el.dates.forEach((elDate) => {
		// 		// 	elDate.start_time = ['10:00', '15:00', '20:00']
		// 		// })
		// 		if (el.dates[0]) {
		// 			if (el.dates[0].start_time && el.dates[0].start_time[0]) {
		// 				el.activeTime = el.dates[0].start_time[0]
		// 			}
		// 		}
		// 	})
		// }
		// console.log('YEAH', res)
		return res
	}).catch(err => console.log(err))
}
const getDirectionBySlug = ($axios, slug) => {
	return $axios.$get('api/v1.0/library/direction/item/by-slug/' + slug).then((res) => {
		// if (res.tours) {
		// 	res.tours.forEach((el) => {
		// 		el.activeTime = null
		// 		el.dates.forEach((elDate) => {
		// 			elDate.start_time = ['10:00', '15:00', '20:00']
		// 		})
		// 		if (el.dates[0]) {
		// 			if (el.dates[0].start_time && el.dates[0].start_time[0]) {
		// 				el.activeTime = el.dates[0].start_time[0]
		// 			}
		// 		}
		// 	})
		// }
		return res
	}).catch((err) => { console.log(err) })
}
export const apiSearch = {
	search,
	getDirectionBySlug
}
