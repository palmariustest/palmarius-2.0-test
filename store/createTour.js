/**
 * @typedef TourInfoHotel
 * @type {object}
 * @property {string} name - название.
 * @property {string} link - ссылка на оьъект.
 */

export class TourInfoHotel {
	constructor(name = '', link = '') {
		this.name = name
		this.link = link
	}
}

/**
 * @typedef TourInfoHabitation
 * @type {object}
 * @property {boolean} includedInPrice - включено в стоимость.
 * @property {TourInfoHotel[]} hotels - гостиницы.
 */

/**
 * @typedef TourInfoFoodType
 * @type {object}
 * @property {string} name - название.
 * @property {string} describe - описание.
 * @property {boolean} isSelected - выбрано.
 */

export class TourInfoFoodType {
	constructor(name = '', describe = '', isSelected = false) {
		this.name = name
		this.describe = describe
		this.isSelected = isSelected
	}
}

/**
 * @typedef TourInfoFood
 * @type {object}
 * @property {boolean} includedInPrice - включено в стоимость.
 * @property {TourInfoFoodType[]} types - типы питания.
 */

/**
 * @typedef feature
 * @type {object}
 * @property {string} name - название, то что после символа '-'.
 * @property {string} describe - описание, то что пользователь добавил в скобках.
 */

/**
 * @typedef TourInfo
 * @type {object}
 * @property {string[]} features - ключевые особенности.
 * @property {string} description - описание тура (html) min=100.
 * @property {TourInfoHabitation} habitation - проживание.
 * @property {TourInfoFood} food - питание.
 * @property {feature[]} featuresIncluded - что входит в стоимость тура.
 * @property {feature[]} featuresNotIncluded - что не входит в стоимость тура.
 */

/**
 * @typedef Tour
 * @type {object}
 * @property {TourInfo} info - Информация о туре.
 * @property {object} program - Программа тура.
 * @property {object} charges - Даты, стоимость и скидки.
 * @property {object} payment - Способ оплаты.
 */

export const createToureLocallyName = 'create-toure'

export const state = () => ({

	/** @type {Tour} */
	tour: {

		/** @type {TourInfo} */
		info: {
			features: [],
			description: '',
			habitation: {
				isIncludedInPrice: false,

				/** @type {TourInfoHotel[]} */
				hotels: []
			},
			food: {
				isIncludedInPrice: false,

				/** @type {TourInfoFoodType[]} */
				types: [
					new TourInfoFoodType(
						'BB',
						'Обычно к трем приемам пищи при подаче типа «шведского стола» предлагается дополнительное питание весь день.',
						false
					),
					new TourInfoFoodType(
						'HB',
						'Обычно к трем приемам пищи.',
						false
					),
					new TourInfoFoodType(
						'FB',
						'Обычно к трем приемам пищи при подаче типа «шведского стола».',
						false
					),
					new TourInfoFoodType(
						'AI',
						'Обычно к трем приемам пищи предлагается дополнительное питание весь день.',
						true
					),
					new TourInfoFoodType(
						'UAI',
						'Обычно предлагается дополнительное питание весь день.',
						false
					)
				]
			},
			featuresIncluded: [],
			featuresNotIncluded: []
		},
		program: { },
		charges: { },
		payment: { }
	}
})

export const mutations = {
	SET_LOCALLY_CREATE_TOUR(state, data) {
		state.tour = data
	},
	SET_INFO_FEATURES(state, data) {
		state.tour.info.features = data
	},
	SET_INFO_DESCRIPTION(state, data) {
		state.tour.info.description = data
	},
	SET_INFO_HABITATION_INCLUDED(state, value) {
		state.tour.info.habitation.isIncludedInPrice = value
	},
	SET_INFO_HOTELS(state, data) {
		state.tour.info.habitation.hotels = data
	},
	SET_INFO_FOOD_INCLUDED(state, value) {
		state.tour.info.food.isIncludedInPrice = value
	},
	SET_INFO_FOOD(state, data) {
		state.tour.info.food.types = data
	},
	SET_INFO_FEATURES_PRESENT(state, data) {
		state.tour.info.featuresIncluded = data
	},
	SET_INFO_FEATURES_ABSENT(state, data) {
		state.tour.info.featuresNotIncluded = data
	}
}

export const actions = {
	SAVE_LOCALLY_CREATE_TOUR({ state }) {
		localStorage.setItem(createToureLocallyName, JSON.stringify(state.tour))
	},
	GET_LOCALLY_CREATE_TOUR({ commit }) {
		let value = null
		try {
			value = JSON.parse(localStorage.getItem(createToureLocallyName))
		} catch (e) {
			localStorage.removeItem(createToureLocallyName)
			return value
		}
		if (!value) {
			return null
		}
		commit('SET_LOCALLY_CREATE_TOUR', value)
		return value
	}
}

export const getters = {
	tourInfoProgress(state) {
		let progress = 0
		const step = 20
		if (state.tour.info.features.length) {
			progress = progress + step
		}
		if (state.tour.info.description) {
			progress = progress + step
		}
		if (state.tour.info.habitation.hotels.length) {
			progress = progress + step
		}
		if (state.tour.info.featuresIncluded.length) {
			progress = progress + step
		}
		if (state.tour.info.featuresNotIncluded.length) {
			progress = progress + step
		}
		return progress
	}
}
