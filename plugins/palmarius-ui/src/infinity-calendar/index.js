import InfinityCalendar from './src/infinity-calendar'

InfinityCalendar.install = function (Vue) {
	Vue.component(InfinityCalendar.name, InfinityCalendar)
}

export default InfinityCalendar
