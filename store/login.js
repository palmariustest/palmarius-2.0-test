// import { registrationApi } from '@/api/registration'

export const state = () => ({
	loginDialog: false
})

export const mutations = {
	SET_DIALOG(state, boolean) {
		state.loginDialog = boolean
	},
	CLEAR(currState) {
		currState = state()
	}
}

export const actions = {
	// async fetchTourBySlug({ commit }, slug) {
	// 	const tour = await toursApi.fetchTour(this.$axios, { slug })
	// 	commit('SET_TOUR', tour)
	// }
}

export const getters = {
	// getters
}
