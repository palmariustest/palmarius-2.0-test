import { apiSearch } from '@/api/search'
import { apiTours } from '@/api/tours'
import { apiPartners } from '@/api/partners'

export const state = () => ({
	pagetitle: null,
	pager: {},
	tours: [],
	myTours: [],
	filterPartner: [],
	filterPartnerPager: {},
	filterStates: {
		vacation: [],
		includes: [],
		tickets: [],
		partners: [],
		price: [1000, 10000]
	},
	queryState: {
		directionSlug: null,
		type: 'both'
	},
	tags: []
})

export const mutations = {
	SET_TOURS(state, data) {
		state.pager = data.pager
		state.tours = data.tours
	},
	ADD_TOURS(state, data) {
		data.tours.forEach((el) => {
			state.tours.push(el)
		})
		state.pager = data.pager
	},
	SET_QUERY_STATE(state, payload) {
		state.queryState[payload.key] = payload.value
	},
	SET_PARTNER_FILTER(state, data) {
		console.log('PFILTER')
		state.filterPartner = data.partners
	},
	SET_TOURS_BY_DIRECTION(state, data) {
		state.pagetitle = data.intl.pagetitle
		state.pager = data.pager
		state.tours = data.tours
	},
	SET_FILTER_STATES(state, data) {
		state.filterStates = data
	},
	CLEAR_FILTERS(currState) {
		currState.filterStates = state().filterStates
	},
	SET_PAGETITLE(state, title) {
		state.pagetitle = title
	},
	SET_TOUR_DAY_TAGS(state, tags) {
		state.tags = tags
	},
	SET_MY_TOURS(state, tours) {
		state.myTours = tours
	},
	CLEAR(currState) {
		currState = state()
	},
	CHOOSE_DATE(state, { tourKey, dateKey }) {
		state.tours[tourKey].activeDate = dateKey
	},
	// изменеие активного дня в турах в account/partner/
	CHOOSE_MY_TOURS_DATE(state, { tourId, dateKey }) {
		const index = state.myTours.findIndex(el => el.id === tourId)
		state.myTours[index].activeDate = dateKey
	}
}

export const actions = {
	async SEARCH_TOURS({ commit, dispatch }, params) {
		const data = await apiSearch.getTours(this.$axios, params.query)
		commit('SET_TOURS', data)
		if (params.withFilter) {
			commit('SET_PARTNER_FILTER', data)
		} else {
			dispatch('GET_PARTNER_FILTER')
			commit('SET_PAGETITLE', data.intl.pagetitle)
		}
	},
	async GET_TOURS({ commit }, query) {
		const data = await apiTours.getTours(this.$axios, query)
		commit('SET_TOURS', data)
	},
	async LOAD_MORE_TOURS({ commit }, query) {
		const data = await apiSearch.getTours(this.$axios, query)
		commit('ADD_TOURS', data)
	},
	async GET_PARTNER_FILTER({ commit }) {
		console.log('WHYYY')
		const data = await apiPartners.getPartnersByTours(this.$axios)
		console.log('PARTNER FILTER', data)
		commit('SET_PARTNER_FILTER', data)
	},
	async GET_TOURS_BY_DIRECTION({ commit }, direction) {
		const data = await apiTours.getToursByDirection(this.$axios, direction, 1000)
		commit('SET_TOURS_BY_DIRECTION', data)
	},
	async GET_TOUR_DAY_TAGS({ commit }) {
		const data = await apiTours.getTourDayTags(this.$axios)

		commit('SET_TOUR_DAY_TAGS', data)
	},
	async GET_MY_TOURS({ commit, rootState }, data) {
		// data { onwner, lang }
		const beforeUpdateAPI = {
			onwner: 3,
			lang: rootState.language

		}
		const dataTours = await apiTours.getMyTours(this.$axios, beforeUpdateAPI)
		commit('SET_MY_TOURS', dataTours.tours)
	},

	CLEAR_FILTERS({ commit, dispatch }) {
		commit('CLEAR_FILTERS')
		this.$cookies.set('filters', state().filterStates)
		dispatch('SEARCH_TOURS', {
			query: {}
		})
	}
}

export const getters = {
	tours(state) {
		return state.tours
	},
	pageTitle(state) {
		return state.intl.pagetitle
	},
	tags(state) {
		return state.tags
	},
	myTours(state) {
		return state.myTours
	}

}
