import { apiRegLogin } from '@/api/regLogin'

export const state = () => ({
	regDialog: false,
	loginDialog: false,
	form: {
		phone: null,
		remember: false,
		name: null,
		family: null,
		password: null,
		email: null,
		role: null,
		code: null,
		company: null
	},
	formLoading: false,
	activeTab: 'VueTypes',
	oldPhone: null
})

export const mutations = {
	SET_REG_DIALOG(state, boolean) {
		state.regDialog = boolean
	},
	SET_LOGIN_DIALOG(state, boolean) {
		state.loginDialog = boolean
	},
	SET_FORM(state, data) {
		state.form = {
			phone: data.phone,
			remember: data.remember,
			name: data.name,
			family: data.family,
			password: data.password,
			email: data.email,
			role: data.role,
			code: data.code
		}
	},
	SET_FORM_LOADING(state, boolean) {
		state.formLoading = boolean
	},
	SET_ROLE(state, newRole) {
		state.form.role = newRole
	},
	SET_TAB(state, tab) {
		state.activeTab = tab
	},
	SET_OLD_PHONE(state, payload) {
		state.oldPhone = payload
	},
	CLEAR(currState) {
		currState = state()
	}
}

export const actions = {
	LOGIN_SUBMIT({ commit, state }) {
		commit('SET_FORM_LOADING', true)
		const bodyFormData = new FormData()
		bodyFormData.append('login', state.form.phone)
		bodyFormData.append('password', state.form.password)
		return apiRegLogin.login(this.$axios, bodyFormData).then((res) => {
			return res
		}).catch((e) => { console.log(e) }).finally(() => {
			commit('SET_FORM_LOADING', false)
		})
	},
	SEND_SMS({ commit, state }) {
		commit('SET_FORM_LOADING', true)
		return apiRegLogin.sendSmsCode(this.$axios, state.form.phone).then((res) => {
			return res
		}).catch((err) => { console.log(err) }).finally(() => {
			commit('SET_FORM_LOADING', false)
		})
	},
	REGISTER({ commit, state }) {
		commit('SET_FORM_LOADING', true)
		return apiRegLogin.register(this.$axios, state.form).then((res) => {
			return res
		}).catch((err) => { console.log(err) }).finally(() => {
			commit('SET_FORM_LOADING', false)
		})
	}
}

export const getters = {
	// getters
}
