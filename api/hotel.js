const getHotels = ($axios, pager) => {
	return $axios.$get('/api/v1.0/partner/hotels', {
		params: pager
	}).then((res) => {
		return res
	}).catch((err) => { console.log(err) })
}
const getHotel = ($axios, slug) => {
	return $axios.$get(`/api/v1.0/hotel/details/${slug}`).then((res) => {
		console.log('CLEAR DATA', res)
		// add mocked data if there are less then 4 services
		// const mockedServises = [
		// 	{ country: 'TH',
		// 		currency: 'USD',
		// 		image: null,
		// 		name: 'Бар',
		// 		tournumber: '5&nbsp;предложений',
		// 		url: '/tours/destination/tailand' },
		// 	{ country: 'IL',
		// 		currency: 'USD',
		// 		image: null,
		// 		name: 'Room сервис',
		// 		tournumber: '38&nbsp;предложений',
		// 		url: '/tours/destination/izrail' },
		// 	{ country: 'RU',1
		// 		currency: 'RUB',
		// 		image: null,
		// 		name: 'Популярное',
		// 		tournumber: '14&nbsp;предложений',
		// 		url: '/tours/destination/irkutsk' },
		// 	{ country: 'RU',
		// 		currency: 'RUB',
		// 		image: null,
		// 		name: 'Экскурсии из отеля',
		// 		tournumber: `${res.xcursions.length} предложений`,
		// 		url: '/tours/destination/sankt-peterburg'
		// 	}
		// ]
		if (!res.service_classes) {
			res.service_classes = []
		}
		if (res.xcursions.length > 0) {
			res.service_classes.push({
				country: 'RU',
				currency: 'RUB',
				image: null,
				name: 'ln_hotel_xcursions',
				tournumber: res.xcursions.length,
				custom_class: 'hotelXcursion',
				url: 'xcursions'
			})
		}

		// mockedServises.forEach((el, key) => {
		// 	if (!res.service_classes[key]) {
		// 		res.service_classes.push(el)
		// 	}
		// })
		return res
	}).catch(err => console.log(err))
}
const hotelServiceAuto = ($axios, hotel, payload) => {
	return $axios.$get('/api/v1.0/hotel/service/auto/' + hotel, {
		params: payload
	}).then(res => res).catch(err => console.log(err))
}
const hotelServiceSearch = ($axios, payload, hotel) => {
	return $axios.$get('/api/v1.0/hotel/service/search/' + hotel, {
		params: payload
	}).then((res) => {
		console.log(res)
		console.warn('HOTEL API', payload, hotel, res)
		if (!res.length) {
			res = []
		}

		// return res.filter(el => el.auto_price)
		return res
	}).catch(err => console.log(err))
}
const hotelServiceDetails = ($axios, payload) => {
	return $axios.$get('/api/v1.0/hotel/service/details/' + payload).then(res => res).catch(err => console.log(err))
}
const hotelServiceBooking = ($axios, payload) => {
	console.log('PAYLOAD', payload)
	return $axios.$post('/api/v1.0/hotel/service/booking', payload).then(res => res).catch(err => console.log(err))
}
const hotelXcursionSearch = ($axios, hotel, payload) => {
	console.log('hotel xcursion search', hotel, payload)
	return $axios.$get('/api/v1.0/hotel/xcursion/search/' + hotel, {
		params: payload
	}).then(res => res).catch(err => console.log(err))
}
export const apiHotel = {
	getHotels,
	getHotel,
	hotelServiceAuto,
	hotelServiceSearch,
	hotelServiceDetails,
	hotelServiceBooking,
	hotelXcursionSearch
}
