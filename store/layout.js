export const state = () => ({
	dialogConfirmShown: false,
	dialogConfirmContent: {
		image: '/static-assets/images/letter.png',
		title: 'ln_common_thankyou',
		message: 'ln_hotel_service_dialog_message',
		button: 'ln_hotel_service_dialog_button'
	}
})
export const mutations = {
	SET_STATES(state, payload) {
		// console.log('STATES', payload)
		const keys = Object.keys(payload)
		keys.forEach((key) => {
			state[key] = payload[key]
		})
	}
}
export const actions = {

}
