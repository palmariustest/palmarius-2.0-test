const getMainpage = ($axios) => {
	// const results = {
	// 	intl: {
	// 		title: '',
	// 		subtitle: ''
	// 	},
	// 	objects: {
	// 		tours: [],
	// 		directions: []
	// 	}
	// }
	return $axios.$get('/api/v1.0/tours/mainpage').then((res) => {
		if (res.objects && res.objects.tours) {
			res.objects.tours.forEach((el) => {
				el.activeTime = null
				// el.dates.forEach((elDate) => {
				// 	elDate.start_time = ['10:00', '15:00', '20:00']
				// })
				if (el.dates[0]) {
					if (el.dates[0].start_time && el.dates[0].start_time[0]) {
						el.activeTime = el.dates[0].start_time[0]
					}
				}
			})
		}
		return res
	}).catch((err) => { console.log(err) })
}
const getMainpagePilgrim = ($axios) => {
	let results = {
		intl: {
			title: '',
			subtitle: ''
		},
		objects: {
			tours: [],
			directions: []
		}
	}
	return $axios.$get('/api/v1.0/tours/mainpage_pilgrim').then((res) => {
		results = res
		console.log('RESLT', res)
		return Promise.resolve(results)
	}).catch((err) => { console.log(err) })
}
export const apiMain = {
	getMainpage,
	getMainpagePilgrim
}
